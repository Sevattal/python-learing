from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import pymysql

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:@127.0.0.1:3308/edu"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "chunge"

# 初始化
db = SQLAlchemy(app)


class Student(db.Model):
    # 配置表名
    __tablename__ = "student"

    # primary_key：主键
    # autoincrement：自增
    # nullable：不能为空
    # Enum：枚举类型
    id = db.Column(db.Integer,
                   primary_key=True,
                   autoincrement=True)
    name = db.Column(db.String(64),
                     nullable=False)
    gender = db.Column(db.Enum("男", "女"),
                       nullable=False)
    phone = db.Column(db.String(11))


# 课程表
class Course(db.Model):
    __tablename__ = "course"
    id = db.Column(db.Integer,
                   primary_key=True,
                   autoincrement=True)
    name = db.Column(db.String(64),
                     nullable=False)
    teacher_id = db.Column(db.Integer,
                           nullable=False)


# 教师表
class Teacher(db.Model):
    __tablename__ = "teacher"
    id = db.Column(db.Integer,
                   primary_key=True,
                   autoincrement=True)
    name = db.Column(db.String(64),
                     nullable=False)
    gender = db.Column(db.Enum("男", "女"),
                       nullable=False)
    phone = db.Column(db.String(11))


# 成绩表
class Grade(db.Model):
    __tablename__ = "grade"
    id = db.Column(db.Integer,
                   primary_key=True,
                   autoincrement=True)
    course_id = db.Column(db.Integer,
                          nullable=False)
    student_id = db.Column(db.Integer,
                           nullable=False)
    grade = db.Column(db.Integer)


# 创建数据表
if __name__ == '__main__':
    db.create_all()
