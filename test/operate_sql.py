from test.models import db, Student

# 增
# s1 = Student(name="zhangsan", gender="男", phone="18621526136")
# s2 = Student(name="lisi", gender="女", phone="18621526136")
# s3 = Student(name="wangwu", gender="男")
# # 单行增加
# db.session.add(s1)
# db.session.commit()
# # 批量增加
# db.session.add_all([s1, s2, s3])
# db.session.commit()

# 查
# get里面放ID,就是再model中定义的id
# stu = Student.query.get(4)
# print(stu.name, stu.gender, stu.phone)
#
# # 查询全部
# stus = Student.query.all()
# for stu in stus:
#     print(stu.name, stu.gender, stu.phone)
#
# # filter() 条件查询
# stus = Student.query.filter(Student.name == "lisi")
# for stu in stus:
#     print(stu.id, stu.name, stu.gender, stu.phone)

# filter_by().all() 代表查询到的全部
# filter_by().first() 代表查询到的第一个
# stus = Student.query.filter_by(name="zhangsan").first()
# print(stus)
#
# # 改
# # 第一种方式
# stu = Student.query.filter(Student.name == "zhangsan").update({"gender": "女"})
# # 显示更新行数
# print(stu)
# db.session.commit()

# 第二种方式
# stus = Student.query.filter(Student.gender == "女").all()
# for stu in stus:
#     stu.gender = "男"
#     db.session.add(stu)
#
# db.session.commit()

# 删
# 第一种方式
stu = Student.query.filter(Student.id <= 2).delete()
print(stu)
db.session.commit()
