from mainapp.models import db
from sqlalchemy import Column
from sqlalchemy import Integer, String


# 声明user模型类，继承db.Model父类
# 默认情况下，类名即为表名
class User(db.Model):
    __tablename__ = "user"
    # 声明字段(属性)，默认情况下属性名与字段相同
    id = Column(Integer,
                primary_key=True,
                autoincrement=True)
    number = Column(String(18))
    name = Column(String(20))
    phone = Column(String(11))
    password = Column(String(100))
