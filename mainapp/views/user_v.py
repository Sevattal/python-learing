from flask import Blueprint
from flask import request, render_template

# user的蓝图创建
blue = Blueprint('userBlue', __name__)


# 用户登陆的视图
@blue.route('/login', methods=['GET', 'POST'])
def login():
    data = {
        'cookies': request.cookies,
        'base_url': request.base_url
    }
    return render_template('user/login.html', **data)
