# 1. 创建连接
from sqlalchemy import create_engine

engine = create_engine('mysql+pymysql://chenk:P@ss123@127.0.0.1:3308/edu?charset=utf8')

# 2. 创建基类, 相当于 django orm中的model.model
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

from sqlalchemy import Column, Integer, String


# 3. 创建表模型
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32))

    def __repr__(self):
        return self.name


Base.metadata.create_all(engine)
